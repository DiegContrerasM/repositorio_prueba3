from django.http import HttpResponse
from django.shortcuts import render,redirect
from appgestion.models import Despacho


def Inicio(request):
    return render(request, "index.html")

def listar(request):
    return render(request, "listar.html")

def busqueda(request):
    return render(request, "busqueda.html")

def ingreso(request):
    return render(request, "ingresar.html")

def eliminar(request):
    return render(request, "eliminar.html")

def modificar(request):
    return render(request, "modificar.html")

    

#acciones de pags

def listar_desp(request):
    datos = Despacho.objects.all()
    return render(request, "listar.html",{'Despacho' :datos})


def ingreso_producto(request):
    numero=request.GET["txt_numDesp"]
    nombreCli=request.GET["txt_cliente"]
    direccion=request.GET["txt_dirección"]
    telefono=request.GET["txt_telefono"]
    producto=request.GET["txt_producto"]
    peso=request.GET["txt_peso"]
    alto=request.GET["txt_alto"]
    ancho=request.GET["txt_ancho"]
    prof=request.GET["txt_profundidad"]
    fecIng=request.GET["txt_fecIng"]
    fecEnv=request.GET["txt_fecEnv"]
    if len(numero)>0 and len(nombreCli)>0 and len(direccion)>0 and len(telefono)>0 and len(producto)>0:
        desp=Despacho(numeroDespacho=numero,nombreCliente=nombreCli,dirCliente=direccion, telCliente=telefono, 
                    productos=producto, peso=peso, alto=alto, ancho=ancho, profundidad=prof,fechaIngreso=fecIng,
                    fechaEnvio=fecEnv,estado='Por enviar')
        desp.save()
        mensaje="Articulo ingresado..."
    else:
        mensaje="Articulo No ingresado o faltan datos..."
    return HttpResponse(mensaje)

def eliminacion_pedido(request):
    if request.GET["txt_numDesp"]:
        numDesp = request.GET["txt_numDesp"]
        despacho = Despacho.objects.filter(numeroDespacho=numDesp)
        if despacho:
            desp=Despacho.objects.get(numeroDespacho=numDesp)
            desp.delete()
            mensaje = "Despacho Eliminado..."
        else:
            mensaje = "Despacho No eliminado...No existe Despacho con ese id"
    else:
        mensaje = "Debe ingresar un id para eliminación..."
    return HttpResponse(mensaje)

def modificar_estado(request):
    if request.GET["txt_numDesp"]:
        num_recibido = request.GET["txt_numDesp"] 
        estado_recibido = request.GET["txt_estado"]
        desp = Despacho.objects.filter(numeroDespacho=num_recibido)
        if desp:
            desp=Despacho.objects.get(numeroDespacho=num_recibido)
            desp.estado = estado_recibido
            desp.save()
            mensaje = "Estado correctamente modificado"
        else:
            mensaje = "No existe despacho a modificar"
    else:
        mensaje = "Debe ingresar un número de desp. para modificar"
    return HttpResponse(mensaje)


