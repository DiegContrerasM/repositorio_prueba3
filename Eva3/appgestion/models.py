from django.db import models
class Despacho(models.Model):
    numeroDespacho=models.IntegerField()
    nombreCliente=models.CharField(max_length=50)
    dirCliente=models.CharField(max_length=90)
    telCliente=models.IntegerField()
    productos=models.CharField(max_length=500)
    peso=models.IntegerField()
    alto=models.IntegerField()
    ancho=models.IntegerField()
    profundidad=models.IntegerField()
    fechaIngreso=models.DateField()
    fechaEnvio=models.DateField()
    estado=models.CharField(max_length=30)

    # Create your models here.
