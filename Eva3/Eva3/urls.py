"""Eva3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
##from appgestion.views import Inicio, listar,listar_desp,busqueda
from appgestion import views
from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/', views.Inicio),
    path('listar/', views.listar),
    path('listar_desp/', views.listar_desp),
    path('busqueda_desp/', views.busqueda),
    path('ingresar/', views.ingreso),
    path('ingreso_producto/',views.ingreso_producto),
    path('eliminar/',views.eliminar),
    path('eliminacion_pedido/', views.eliminacion_pedido),
    path('modificar/', views.modificar),
    path('modificar_estado/', views.modificar_estado),

]
